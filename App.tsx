import 'react-native-gesture-handler';

import React from 'react';
import { AuthContextProvider } from './src/context/AuthContext';
import MainRouter from './src/navigators/MainRouter';
import { StoreProvider } from 'easy-peasy';
import store from './src/stores/store/index'

export default function App() {
  return (
      <AuthContextProvider>
        <StoreProvider store={store}>
          <MainRouter />
        </StoreProvider>
      </AuthContextProvider>
  )
}
