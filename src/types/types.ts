export type Expense = {
	id?: string
	name: string
	isCredit: boolean
	createdAt: number
	amount: number
	explanation: string
	idCategory: string
}

export type Category = {
	id?: string
	name: string
	icon: string
	typeIcon: string
	color: string
	isNative: boolean
}

export type Profile = {
	id?: string
	firstName: string
	lastName: string
	age: number
	email: string
}
