import { StackNavigationProp } from '@react-navigation/stack';
import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Text, FlatList, TouchableOpacity } from 'react-native';
import { DashboardStackParamList } from '../navigators/MainRouter';
import { Category, Expense } from "../types/types"
import Ionicons from 'react-native-vector-icons/Ionicons';
import ExpenseList from '../components/ExpenseList';
import { useIsFocused } from '@react-navigation/native';
import { useStoreState } from '../stores/hooks';

type Props = {
	navigation: StackNavigationProp<DashboardStackParamList>
}

const FilterScreen: React.FC<Props> = ({navigation}) => {
	const [currentCategory, setCurrentCategory] = useState<number>(0)
	const [displayExpense, setDisplayExpense] = useState<Expense[]>([])

	const categories = useStoreState(state => state.categories.categories)
	const expenses = useStoreState(state => state.expenses.expenses)
	const isFocused = useIsFocused()

	useEffect(() => {
		displayCategory(categories[currentCategory])
	}, [isFocused]);

	const displayCategory = (category:Category) => {
		setDisplayExpense(expenses.filter(expense => expense.idCategory == category.id))
	}

	return (
		<View style={styles.container}>
			<Text style={styles.titleCategories}>Categories</Text>
			<View style={styles.containerCategories}>
				<FlatList
					horizontal={true}
					data = {categories}
					renderItem = {({item, index}) => 
						<TouchableOpacity style={[styles.containerCategory, {backgroundColor: item.color}]} onPress={() => {
							displayCategory(item)
							setCurrentCategory(index)
						}}>
							<Text style={styles.titleCategory}>{item.name}</Text>
							<View>
								<Ionicons name={item.icon} size={45} color={"black"} />
							</View>
						</TouchableOpacity>
					}
					keyExtractor = {((item) => item.id + "")}
				/>
			</View>

			<View style={{flex:1, flexGrow:1, flexShrink:1, paddingBottom:50}}>
				<ExpenseList navigation={navigation} expenses={displayExpense} /> 
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flexDirection: 'column',
		height: "100%",
		backgroundColor: '#303236'
	},
	title: {
		fontSize: 30,
		fontWeight: 'bold',
		marginLeft: 10,
		color: 'white'
	},
	titleCategories: {
		fontSize: 30,
		fontWeight: 'bold',
		paddingLeft: 10,
		color: 'white',
		backgroundColor: '#474a4f'
	},
	containerCategories: {
		backgroundColor: '#474a4f'
	},
	containerCategory: {
		alignItems: "flex-start",
		flexDirection: "column",
		justifyContent: "space-between",
		width: 120,
		height: 200,
		margin: 10,
		padding: 10,
		borderRadius: 20,
	},
	titleCategory: {
		fontSize: 27,
		fontWeight: "bold"
	},
})

export default FilterScreen
