import { RouteProp, useIsFocused } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import React, { useState, useEffect } from 'react';

import InputSpinner from 'react-native-input-spinner';
import DropDownPicker from 'react-native-dropdown-picker';

import { SafeAreaView, TouchableOpacity, StyleSheet, Text, TextInput, View, Alert } from 'react-native';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import { DashboardStackParamList } from '../navigators/MainRouter';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { useStoreActions, useStoreState } from '../stores/hooks';
import { Expense } from '../types/types';

type Props = {
	navigation: StackNavigationProp<DashboardStackParamList, 'ExpenseScreen'>
	route: RouteProp<DashboardStackParamList, 'ExpenseScreen'>
}

const radio_credit = [
	{ label: 'Is expense', value: 0 },
	{ label: 'Is credit', value: 1 }
]

const ExpenseScreen: React.FC<Props> = ({ navigation, route }) => {
	const { expense } = route.params
	const [stateExpense, setStateExpense] = useState<Expense>(expense)
	const [dropDownItem, setDropDownItem] = useState<any[]>([])
	const [isNewExpense, setIsNewExpense] = useState<boolean>(true)

	const categories = useStoreState(state => state.categories.categories)

	const createExpense = useStoreActions(actions => actions.expenses.createExpense)
	const updateExpense = useStoreActions(actions => actions.expenses.updateExpense)
	const deleteExpense = useStoreActions(actions => actions.expenses.deleteExpense)

	const isFocused = useIsFocused()

	useEffect(() => {
		const tabItem: any[] = []
		categories.forEach(category => {
			tabItem.push({ label: category.name, value: category.id, icon: () => <Ionicons name={category.icon} size={25} color={"black"} /> })
		})
		setDropDownItem(tabItem)

		// Disable delete button if it's new expense
		if (expense.id!) {
			setIsNewExpense(false);
		}
	}, [isFocused])

	const onPressSave = async () => {
		if (!stateExpense.name || !stateExpense.amount || !stateExpense.idCategory) {
			Alert.alert('Please fill in the fields')
		} else {
			if (isNewExpense) {
				createExpense(stateExpense)
			} else {
				updateExpense({...stateExpense, id: expense.id!})
			}
			navigation.goBack()
		}
	}

	const onPressDelete = async () => {
		deleteExpense({id:expense.id!})
		navigation.goBack()
	}

	const isSelectRadio = (index: number) => {
		if (index == 0 && ! stateExpense.isCredit)
			return true
		if (index == 1 && stateExpense.isCredit)
			return true
		return false
	}

	return (
		<SafeAreaView style={styles.container}>

			<Text style={styles.label}>Name *</Text>
			<TextInput
				defaultValue={stateExpense.name}
				style={styles.input}
				onChangeText={(text) => {
					stateExpense.name = text
				}}
			/>

			<Text style={styles.label}>Amount type *</Text>
			<View style={{ marginBottom: 16 }}>
				<RadioForm formHorizontal={true} animation={true}>
					{
						radio_credit.map((obj, i) => (
							<RadioButton labelHorizontal={true} key={i} >
								<RadioButtonInput
									obj={obj}
									index={i}
									isSelected={isSelectRadio(i)}
									onPress={(value) => { setStateExpense({...stateExpense, isCredit: value}) }}
									buttonSize={10}
									buttonOuterSize={20}
								/>
								<RadioButtonLabel
									obj={obj}
									index={i}
									labelHorizontal={true}
									labelStyle={{ fontSize: 18, color: 'white' }}
									labelWrapStyle={{ marginRight: 30 }}
									onPress={(value) => { setStateExpense({...stateExpense, isCredit: value}) }}
								/>
							</RadioButton>
						))
					}
				</RadioForm>
			</View>

			<Text style={styles.label}>Amount *</Text>
			<InputSpinner
				style={styles.inputSpinner}
				max={10000}
				min={0}
				step={1}
				precision={2}
				background={'transparent'}
				colorMax={'#f04048'}
				colorMin={'#40c5f4'}
				color={"#d3d7db"}
				value={stateExpense.amount}
				onChange={(num) => setStateExpense({...stateExpense, amount: num})}
				buttonStyle={{ height: 40, width: 40 }}
				buttonFontSize={25}
				fontSize={20}
				buttonTextColor={'black'}
				textColor={'white'}
			/>

			<Text style={styles.label}>Explanation</Text>
			<TextInput
				multiline={true}
				numberOfLines={3}
				style={[styles.input, { textAlignVertical: 'top' }]}
				defaultValue={expense.explanation}
				onChangeText={(text) => setStateExpense({...stateExpense, explanation: text})}
				placeholder={"Explanation about your expense or your credit"}
			/>

			<Text style={styles.label}>Category *</Text>
			<View style={{ marginBottom: 16 }}>
				<DropDownPicker
					items={dropDownItem}
					defaultValue={dropDownItem.length != 0 ? expense.idCategory : ""}
					containerStyle={{ height: 40 }}
					selectedLabelStyle={{ color: 'black' }}
					style={{ backgroundColor: "#d3d7db" }}
					itemStyle={{ justifyContent: 'flex-start' }}
					labelStyle={{ fontSize: 16, color: "black" }}
					dropDownStyle={{ backgroundColor: '#ebebeb' }}
					onChangeItem={item => setStateExpense({...stateExpense, idCategory: item.value})}
					placeholder={"Select a category"}
				/>
			</View>

			<View style={styles.containerButtons}>
				<TouchableOpacity style={[styles.button, styles.buttonSave]} onPress={onPressSave}>
					<Text style={styles.buttonText}>Save</Text>
				</TouchableOpacity>
				<TouchableOpacity disabled={isNewExpense}
					style={[styles.button, isNewExpense ? styles.buttonDisable : styles.buttonDelete]}
					onPress={onPressDelete}>
					<Text style={styles.buttonText}>Delete</Text>
				</TouchableOpacity>
			</View>

		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	container: {
		width: '100%',
		height: '100%',
		padding: 15,
		backgroundColor: '#474a4f'
	},
	label: {
		fontSize: 20,
		marginBottom: 5,
		color: 'white'
	},
	input: {
		width: "100%",
		fontSize: 18,
		borderWidth: 1,
		marginBottom: 16,
		color: 'black',
		borderRadius: 10,
		backgroundColor: '#d3d7db'
	},
	inputSpinner: {
		width: "100%",
		marginBottom: 22,
	},
	containerButtons: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		flexShrink: 1
	},
	button: {
		borderRadius: 10,
		height: 40,
		width: "100%",
		alignItems: "center",
		justifyContent: "center",
		flexShrink: 1,
	},
	buttonSave: {
		backgroundColor: '#42f545',
	},
	buttonDelete: {
		backgroundColor: '#f32821',
		marginLeft: 10
	},
	buttonDisable: {
		backgroundColor: '#AAAAAA',
		marginLeft: 10
	},
	buttonText: {
		fontSize: 17,
		fontWeight: 'bold',
	},
})

export default ExpenseScreen
