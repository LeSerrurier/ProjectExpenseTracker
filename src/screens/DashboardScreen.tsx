import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import { StackNavigationProp } from '@react-navigation/stack';
import { DashboardStackParamList } from '../navigators/MainRouter';

import ExpenseList from '../components/ExpenseList';
import ExpenseChart from '../components/ExpenseChart';

import { Expense } from '../types/types';
import { useStoreState } from '../stores/hooks'

type Props = {
	navigation: StackNavigationProp<DashboardStackParamList>
}

const DashboardScreen: React.FC<Props> = ({navigation}) => {
	const expenses = useStoreState(state => state.expenses.expenses)
	
	const onPressAddExpense = () => {
		const expense: Expense = {
			name: '',
			isCredit: false,
			createdAt: 0,
			amount: 0,
			explanation: '',
			idCategory: '',
		}
		navigation.navigate('ExpenseScreen', {expense})
	}

	return (
		<View style={styles.container}>
			<ExpenseChart />
			<View style={{flex:1, paddingBottom:50}}>
				<ExpenseList navigation={navigation} expenses={expenses} /> 
			</View>
			<View style={styles.containerButtonAdd}>
				<TouchableOpacity style={styles.buttonAdd} onPress={() => onPressAddExpense()}>
					<MaterialIcons name="add" size={25} color="white" />
				</TouchableOpacity>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flexDirection: 'column',
		height: "100%",
		backgroundColor: '#303236'
	},
	title: {
		fontSize: 40,
		fontWeight: '700',
		marginBottom: 16,
		textAlign: "center",
		color: "black"
	},
	content: {
		flex: 1,
	},
	input: {
		height: 500,
		width: 300,
		fontSize: 24,
		borderWidth: 1,
		marginBottom: 16,
	},
	containerButtonAdd: {
		position:"absolute",
		bottom: 10,
		right: 10
	},
	buttonAdd: {
		borderWidth:1,
		borderColor:'rgba(0,0,0,0.2)',
		width: 50,
		height: 50,
		backgroundColor:'green',
		borderRadius:100,
		justifyContent: "center",
		alignItems:'center',
	}
})

export default DashboardScreen
