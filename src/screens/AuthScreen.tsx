import React, { useState } from 'react';
import { SafeAreaView, Text, TextInput, StyleSheet, Alert, View, Image, TouchableOpacity } from 'react-native';
import { useAuth } from '../context/AuthContext';
import InputSpinner from 'react-native-input-spinner';
import { Profile } from '../types/types'
import { ScrollView } from 'react-native-gesture-handler';

const profileTemp: Profile = {
	email: '',
	firstName: '',
	lastName: '',
	age: 0
}

const AuthScreen = () => {
	const auth = useAuth()
	const [isRegister, setIsRegister] = useState(false)
	const [password, setPassword] = useState<string>('')
	const [profile, setProfile] = useState<Profile>(profileTemp)

	const onPressAuth = async () => {
		if (!profile.email || !password) {
			Alert.alert('Please fill in the fields')
			return
		}

		if (isRegister) {
			try {
				await auth.register(profile.email, password, profile.firstName, profile.lastName, profile.age).then(() => {
					Alert.alert('Account was created')
					setPassword('')
					setProfile(profileTemp)
				})
			} catch (e) {
				if (e.code === 'auth/invalid-email') {
					Alert.alert("Invalid email")
				} else if (e.code === 'auth/weak-password') {
					Alert.alert("Password must respect minimum size (8)")
				} else {
					Alert.alert(e.message)
					console.warn(e.code)
				}
			}
		}

		if (!isRegister) {
			try {
				await auth.signIn(profile.email, password);
			} catch (e) {
				if (e.code === 'auth/user-not-found') {
					Alert.alert("Account don\'t exist")
				} else if (e.code === 'auth/invalid-email') {
					Alert.alert("Invalid email")
				} else if (e.code === 'auth/wrong-password') {
					Alert.alert("Wrong password")
				} else {
					Alert.alert(e.message)
					console.warn(e.code)
				}
			}
		}
	}

	const toggleRegister = () => {
		setIsRegister(!isRegister)
	}

	const displayForm = () => {
		if(isRegister)
			return (
				<>
					<Text style={styles.label}>First name</Text>
					<TextInput value={profile.firstName} onChangeText={(text) => setProfile({...profile, firstName: text})} style={styles.input} />
					<Text style={styles.label}>Last name</Text>
					<TextInput value={profile.lastName} onChangeText={(text) => setProfile({...profile, lastName: text})} style={styles.input} />
					<Text style={styles.label}>Age</Text>
					<InputSpinner
						style={styles.input}
						max={90}
						min={0}
						step={1}
						colorMax={"#f04048"}
						colorMin={"#40c5f4"}
						value={profile.age}
						onChange={(num) => setProfile({...profile, age: num})}
						buttonStyle={{height:40, width:40}}
						buttonFontSize={25}
						fontSize={20}
					/>
				</>
			)
	}

	return (
		<SafeAreaView style={styles.container}>
			<ScrollView>
				<View style={styles.containerLogoAndText}>
					<View style={styles.containerLogo}>
						<Image style={styles.logo} source={require('../images/LogoApp.png')}/>
					</View>
					<Text style={styles.title}>Expense Tracker Pro</Text>
				</View>

				<View style={styles.containerForm}>

					<Text style={styles.label}>Email</Text>
					<TextInput value={profile.email} onChangeText={(text) => setProfile({...profile, email: text})} style={styles.input} />
					<Text style={styles.label}>Password</Text>
					<TextInput value={password} onChangeText={setPassword} style={styles.input} secureTextEntry={true} />

					{displayForm()}

				</View>

				<TouchableOpacity style={styles.logInButton} onPress={onPressAuth}>
					<Text style={styles.buttonText}>{isRegister ? 'Register' : 'Log In'}</Text>
				</TouchableOpacity>

				<Text style={styles.label}>
					{isRegister ? 'Already have an account ? ' : "You don't have an account?"}
				</Text>
				<TouchableOpacity style={styles.logInButton} onPress={toggleRegister}>
					<Text style={styles.buttonText}>{isRegister ? 'Log in here ' : 'Register here'}</Text>
				</TouchableOpacity>
			</ScrollView>
		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	container: {
		padding: 20
	},
	containerLogoAndText: {
		marginBottom: 25,
	},
	containerLogo: {
		height: 110,
	},
	logo: {
		width: '100%',
		height: '100%',
		resizeMode: "contain",
		justifyContent: "center",
	},
	title: {
		fontSize: 36,
		fontWeight: "bold",
		textAlign: "center",
	},
	containerForm: {
		flexDirection: "column"
	},
	label: {
		fontSize: 16,
		marginBottom: 5
	},
	input: {
		width: "100%",
		fontSize: 18,
		borderWidth: 1,
		marginBottom: 16,
		borderRadius:15
	},
	logInButton: {
		marginBottom: 32,
		borderRadius:15,
		height: 40,
		justifyContent: 'center',
		alignItems: "center",
		backgroundColor: '#2196F3'
	},
	buttonText: {
		color: 'white',
		fontSize: 17,
		fontWeight: 'bold'
	},
})

export default AuthScreen
