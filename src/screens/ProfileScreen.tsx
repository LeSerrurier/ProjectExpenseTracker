import { StackNavigationProp } from '@react-navigation/stack';
import React from 'react';
import { SafeAreaView, View, StyleSheet, Text, TextInput, TouchableOpacity, Alert } from 'react-native';

import { DashboardStackParamList } from '../navigators/MainRouter';

import { useAuth } from '../context/AuthContext';
import InputSpinner from 'react-native-input-spinner';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { useStoreState, useStoreActions } from '../stores/hooks'

type Props = {
	navigation: StackNavigationProp<DashboardStackParamList>
}

const CredentialScreen: React.FC<Props> = ({ navigation }) => {
	const auth = useAuth();
	const profile = useStoreState(state => state.profiles.profiles)[0]

	const updateProfile = useStoreActions(actions => actions.profiles.updateProfile)
	const deleteProfile = useStoreActions(actions => actions.profiles.deleteProfile)

	const onPressSave = async () => {
		if (!profile.firstName || !profile.lastName || !profile.age) {
			Alert.alert('Please fill in the fields')
		} else {
			updateProfile({ ...profile, id: profile.id! })
			navigation.navigate('DashboardScreen')
		}
	}

	const onPressDelete = async () => {
		if (profile.id!) {
			deleteProfile()
		}
	}

	const onPressLogOut = () => {
		auth.signOut()
	}

	return (
		<SafeAreaView style={styles.container}>

			<Text style={styles.label}>Email</Text>
			<TextInput
				defaultValue={profile.email}
				style={[styles.input, { color: '#5e5b5b' }]}
				onChangeText={(text) => {
					profile.email = text
				}}
				editable={false}
			/>

			<Text style={styles.label}>First name</Text>
			<TextInput
				defaultValue={profile.firstName}
				style={styles.input}
				onChangeText={(text) => {
					profile.firstName = text
				}}
			/>
			
			<Text style={styles.label}>Last name</Text>
			<TextInput
				defaultValue={profile.lastName}
				style={styles.input}
				editable={true}
				onChangeText={(text) => {
					profile.lastName = text
				}}
			/>

			<Text style={styles.label}>Age</Text>
			<InputSpinner
				style={styles.inputSpinner}
				max={90}
				min={0}
				step={1}
				colorMax={"#f04048"}
				colorMin={"#40c5f4"}
				color={"#d3d7db"}
				value={profile.age}
				onChange={(num) => profile.age = num}
				buttonStyle={{ height: 40, width: 40 }}
				buttonFontSize={25}
				fontSize={20}
				buttonTextColor={'black'}
				textColor={'white'}
			/>

			<View style={styles.containerButtons}>
				<TouchableOpacity style={[styles.button, styles.buttonSave]} onPress={onPressSave}>
					<Text style={styles.buttonText}>Save</Text>
				</TouchableOpacity>
				<TouchableOpacity style={[styles.button, styles.buttonDelete]} onPress={onPressDelete}>
					<Text style={styles.buttonText}>Delete</Text>
				</TouchableOpacity>
				<TouchableOpacity style={[styles.button, styles.buttonDisconnect]} onPress={onPressLogOut}>
					<FontAwesome name="sign-out" size={25} color='white' />
				</TouchableOpacity>
			</View>

		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	container: {
		height: '100%',
		padding: 20,
		backgroundColor: '#474a4f'
	},
	label: {
		fontSize: 20,
		marginBottom: 5,
		color: 'white'
	},
	input: {
		width: "100%",
		fontSize: 18,
		borderWidth: 1,
		marginBottom: 16,
		color: 'black',
		borderRadius: 10,
		backgroundColor: '#d3d7db'
	},
	inputSpinner: {
		width: "100%",
		marginBottom: 22,
	},
	containerButtons: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		flexShrink: 1
	},
	button: {
		borderRadius: 10,
		height: 40,
		width: "100%",
		alignItems: "center",
		justifyContent: "center",
		flexShrink: 1,
		color: 'white'
	},
	buttonSave: {
		backgroundColor: '#42f545',
	},
	buttonDelete: {
		backgroundColor: '#f32821',
		marginLeft: 10
	},
	buttonDisconnect: {
		backgroundColor: "#2196F3",
		width: 200,
		marginLeft: 80
	},
	buttonText: {
		color: 'black',
		fontSize: 17,
		fontWeight: 'bold'
	},
})

export default CredentialScreen
