import React, { useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { useAuth } from '../context/AuthContext';
import { Expense } from '../types/types';
import { useStoreActions } from '../stores/hooks'

import AuthScreen from '../screens/AuthScreen';
import DashboardScreen from '../screens/DashboardScreen';
import ExpenseScreen from '../screens/ExpenseScreen';
import FilterScreen from '../screens/FilterScreen';
import ProfileScreen from '../screens/ProfileScreen';

export type DashboardStackParamList = {
	DashboardScreen: undefined,
	ExpenseScreen: {expense: Expense}
}

export type FilterStackParamList = {
	FilterScreen: undefined,
	ExpenseScreen: {expense: Expense}
}

export type MainTabParamList = {
	DashboardScreen: DashboardStackParamList,
	FilterScreen: FilterStackParamList,
	ProfileScreen: undefined
}

const DashboardStack = createStackNavigator<DashboardStackParamList>()
const FilterStack = createStackNavigator<FilterStackParamList>()
const Tab = createBottomTabNavigator<MainTabParamList>()

const MainStackNavigator = () => {
	return (
		<DashboardStack.Navigator>
			<DashboardStack.Screen name="DashboardScreen" options={{headerShown: false}} component={DashboardScreen} />
			<DashboardStack.Screen name="ExpenseScreen" component={ExpenseScreen} 
				options={{
					title:"Expense Screen",
					headerTitleAlign: 'center',
					headerStyle: {
						backgroundColor: 'grey' 
					},
					headerTintColor: '#fff',
				}}
			/>
		</DashboardStack.Navigator>
	)
}

const FilterStackNavigator = () => {
	return (
		<FilterStack.Navigator>
			<FilterStack.Screen name="FilterScreen" options={{headerShown: false}} component={FilterScreen} />
			<FilterStack.Screen name="ExpenseScreen" component={ExpenseScreen} 
				options={{
					title:"Expense Screen",
					headerTitleAlign: 'center',
					headerStyle: {
						backgroundColor: 'grey' 
					},
					headerTintColor: '#fff',
				}}
			/>
		</FilterStack.Navigator>
	)
}


export default function MainRouter() {
	const auth = useAuth()
	const listenExpenses = useStoreActions(actions => actions.expenses.listenExpenses)
	const listenCategories = useStoreActions(actions => actions.categories.listenCategories)
	const listenProfiles = useStoreActions(actions => actions.profiles.listenProfiles)

	useEffect(() => {
		if(auth.isSignedIn) {
			listenExpenses()
			listenCategories()
			listenProfiles()
		}
	}, [auth.isSignedIn])

	const renderRoutes = () => {
		if (!auth.isSignedIn) {
			return <AuthScreen />
		}

		if (auth.isSignedIn) {

			return (
				<Tab.Navigator tabBarOptions={{
					labelStyle : {
						fontSize: 15
					},
					style: {
						backgroundColor: "white",
						borderTopColor: "black",
						borderTopWidth: 1,
						padding: 5
					},
				}}>
					<Tab.Screen name="DashboardScreen" component={MainStackNavigator} options={{
						tabBarLabel: 'Dashboard',
						tabBarIcon: ({ color, size }) => (
							<MaterialCommunityIcons name="monitor-dashboard" size={size} color={color} />
						)
					}}/>
					<Tab.Screen name="FilterScreen" component={FilterStackNavigator} options={{
						tabBarLabel: 'Filter',
						tabBarIcon: ({ color, size }) => (
							<FontAwesome name="filter" size={size} color={color} />
						)
					}}/>
					<Tab.Screen name="ProfileScreen" component={ProfileScreen} options={{
						tabBarLabel: 'Profile',
						tabBarIcon: ({ color, size }) => (
							<FontAwesome name="user-circle-o" size={size} color={color} />
						)
					}}/>
				</Tab.Navigator>
			)
		}
	}

	return (
		<NavigationContainer>
			{renderRoutes()}
		</NavigationContainer>
	)
}
