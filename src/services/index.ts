import * as profile from './profile';
import * as expenses from './expenses';

const services = {
	profile,
	expenses,
}

export default services
