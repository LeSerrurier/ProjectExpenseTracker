import firestore from '@react-native-firebase/firestore';
import { firebase } from '@react-native-firebase/auth';
import { EXPENSES_COLLECTION, USERS_COLLECTION } from '../constants';
import { Expense } from './../types/types';

const expenses = () =>
	firestore()
		.collection(USERS_COLLECTION)
		.doc(firebase.auth().currentUser?.uid)
		.collection(EXPENSES_COLLECTION)

export const getExpenses: () => Promise<Expense[]> = async () => {
	const querySnapshot = await expenses().get()
	const docs = querySnapshot.docs

	return docs.map((doc) => {
		const data = doc.data() as Expense
		return {...data, id: doc.id}
	})
}

export const createExpense: (
	name: string,
	isCredit: boolean,
	amount: number,
	explanation: string,
	idCategory: string,
) => Promise<Expense> = async (name, isCredit, amount, explanation ,idCategory) => {
	const expense: Expense = {
		name: name,
		isCredit: isCredit,
		createdAt: new Date().getTime(),
		amount: amount,
		explanation: explanation,
		idCategory: idCategory,
	};
	const doc = await expenses().add(expense)
	return {...expense, id: doc.id}
}

export const updateExpense: (
	expenseId: string,
	name: string,
	isCredit: boolean,
	amount: number,
	explanation: string,
	idCategory: string,
) => Promise<void> = async (expenseId, name, isCredit, amount, explanation, idCategory) => {
	await expenses().doc(expenseId).update({
		name: name,
		isCredit: isCredit,
		amount: amount,
		explanation: explanation,
		idCategory: idCategory,
	})
}

export const deleteExpense: (expenseId: string) => Promise<void> = async (expenseId) => {
	await expenses().doc(expenseId).delete()
}
