import firestore from '@react-native-firebase/firestore';
import { firebase } from '@react-native-firebase/auth';
import { PROFILE_COLLECTION, USERS_COLLECTION } from '../constants';
import { Profile } from '../types/types';
import { Alert } from 'react-native';

// Get Profile from firebase
const profile = () =>
	firestore()
		.collection(USERS_COLLECTION)
		.doc(firebase.auth().currentUser?.uid)
		.collection(PROFILE_COLLECTION)

export const getProfile: () => Promise<Profile> = async () => {
	const querySnapshot = await profile().get()
	const docs = querySnapshot.docs

	return { ...docs[0].data(), id: docs[0].id } as Profile
}

export const getProfiles: () => Promise<Profile[]> = async () => {
	const querySnapshot = await firestore().collection(USERS_COLLECTION).get()
	const docs = querySnapshot.docs
	return docs.map((doc) => {
		const data = doc.data() as Profile
		return { ...data, id: doc.id }
	})
}

export const createProfile: (
	firstName: string,
	lastName: string,
	age: number,
	email: string,
) => Promise<Profile> = async (firstName, lastName, age, email) => {
	const profileToAdd: Profile = {
		firstName: firstName,
		lastName: lastName,
		age: age,
		email: email,
	}
	const doc = await profile().add(profileToAdd);
	return { ...profileToAdd, id: doc.id }
}

export const updateProfile: (
	profileId: string,
	firstName: string,
	lastName: string,
	age: number,
) => Promise<void> = async (profileId, firstName, lastName, age) => {
	await profile().doc(profileId).update({
		firstName: firstName,
		lastName: lastName,
		age: age,
	})
}

export const deleteProfile: () => Promise<void> = async () => {
	await firebase.auth().currentUser?.delete().then(() => {
		firestore().collection(USERS_COLLECTION).doc(firebase.auth().currentUser?.uid).delete().then().catch(function (error) {
			Alert.alert("Error has occurred during account deletion")
		})
	})
}
