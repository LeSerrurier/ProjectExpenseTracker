import React, { useEffect, useState } from 'react';
import { Dimensions } from 'react-native';
import { PieChart } from 'react-native-chart-kit'
import { useStoreState } from '../stores/hooks'

const monthNumber = 6
const startDate = new Date().setMonth(new Date().getMonth() - (monthNumber - 1))

const ChartRepartition = () => {
	const [data, setData] = useState<any[]>([])

	const expenses = useStoreState(state => state.expenses.expenses)
	const categories = useStoreState(state => state.categories.categories)

	useEffect(() => {
		formatData()
	}, [expenses, categories])

	const formatData = () => {
		const tempDataAmount = new Array(categories.length).fill(0)
		const validExpenses = expenses.filter(expense => expense.createdAt >= startDate)
		validExpenses.forEach(expense => {
			let tempExpense = 0
			expense.isCredit ? tempExpense += 0 : tempExpense += expense.amount
			tempDataAmount[categories.findIndex(_category => _category.id == expense.idCategory)] += tempExpense
		})
		const tempData: any[] = []
		tempDataAmount.forEach((_amount, index) =>
			tempData.push({
				name: categories[index].name,
				amount: _amount,
				color: categories[index].color,
				legendFontColor: 'white',
				legendFontSize: 16,
			})
		)
		setData(tempData)
	}

	return (
		<PieChart
			data={data}
			width={Dimensions.get('window').width}
			height={220}
			chartConfig={{
				backgroundColor: '#1cc910',
				backgroundGradientFrom: '#eff3ff',
				backgroundGradientTo: '#efefef',
				decimalPlaces: 2,
				color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
				style: {
					borderRadius: 16,
				},
			}}
			style={{
				marginVertical: 8,
				borderRadius: 16,
			}}
			accessor="amount"
			backgroundColor="transparent"
			paddingLeft="0"
		/>
	)
}

export default ChartRepartition;
