import React from 'react';
import { StyleSheet, View, Text, ScrollView } from 'react-native';
import ChartEvolution from '../components/ChartEvolution';
import ChartRepartition from '../components/ChartRepartition';

const ExpenseChart = () => {
	return (
		<View style={styles.container}>
			<ScrollView horizontal={true}>
				<View style={styles.chartContainer}>
					<Text style={styles.titleChart}>Expense and credit evolution</Text>
					<ChartEvolution />
				</View>
				<View style={styles.chartContainer}>
					<Text style={[styles.titleChart,{paddingLeft:5}]}>Expense repartition</Text>
					<ChartRepartition />
				</View>
			</ScrollView>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: '#474a4f'
	},
	titleChart: {
		color:'white',
		fontSize: 25,
		fontWeight: 'bold'
	},
	chartContainer: {
		paddingLeft:4
	}
})

export default ExpenseChart;