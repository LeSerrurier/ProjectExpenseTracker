import { useIsFocused } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { Dimensions } from 'react-native';
import { LineChart } from 'react-native-chart-kit'
import { useStoreState } from '../stores/hooks'

const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
const monthNumber = 6
const startDate = new Date().setMonth(new Date().getMonth() - (monthNumber - 1))

const ChartEvolution = () => {
	const [labels, setLabels] = useState<string[]>([])
	const [data, setData] = useState<number[]>(new Array(monthNumber).fill(0))
	const isFocused = useIsFocused()

	const expenses = useStoreState(state => state.expenses.expenses)

	useEffect(() => {
		formatLabel()
	}, [isFocused])

	useEffect(() => {
		formatData()
	},[expenses])

	const formatData = () => {
		const tempData = new Array(monthNumber).fill(0)
		const validExpenses = expenses.filter(expense => expense.createdAt >= startDate)
		validExpenses.forEach(expense => {
			let tempExpense = 0
			expense.isCredit ? tempExpense += expense.amount : tempExpense -= expense.amount
			tempData[((new Date(expense.createdAt).getMonth() - new Date(startDate).getMonth())+12)%12] += tempExpense
		})
		setData(tempData)
	}

	const formatLabel = () => {
		const currentMonth = new Date(startDate).getMonth()
		const tempLabels = []
		for(let i = 0; i < monthNumber; i++) {
			tempLabels.push(monthNames[(currentMonth + i)%12].slice(0,3))
		}
		setLabels(tempLabels)
	}

	return (
		<LineChart
			data={{
				labels: labels,
				datasets: [{data: data}]
			}}
			width={Dimensions.get("window").width-5}
			height={220}
			yAxisSuffix="$"
			yAxisInterval={1}
			chartConfig={{
				backgroundColor: "#e26a00",
				backgroundGradientFrom: "#fb8c00",
				backgroundGradientTo: "#ffa726",
				decimalPlaces: 0,
				color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
				labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
				style: {
					borderRadius: 16,
				},
				propsForDots: {
					r: "6",
					strokeWidth: "2",
					stroke: "#ffa726"
				},
				propsForLabels: {fontSize:"12", fontWeight:"bold"}
			}}
			bezier
			style={{
				marginVertical: 8,
				borderRadius: 16
			}}
		/>
	)
}

export default ChartEvolution;
