import React, { useEffect, useState } from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import { StyleSheet, View, Text, FlatList, TouchableOpacity } from 'react-native';

import { Expense } from '../types/types';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { useStoreState } from '../stores/hooks';

type Props = {
	navigation: StackNavigationProp<any>
	expenses: Expense[]
}

type SortType = {
	name: string,
	state: number
}

const ExpenseList: React.FC<Props> = ({ navigation, expenses }) => {
	const [lineExplanation, setLineExplanation] = useState<number[]>([])
	const [stateSort, setStateSort] = useState<SortType>({name:"Date", state: 0})

	const categories = useStoreState(state => state.categories.categories)

	expenses.sort((expenseA, expenseB) => expenseB.createdAt - expenseA.createdAt)

	useEffect(() => {
		setLineExplanation(new Array(expenses.length).fill(1))
	}, [expenses])

	// Compute total amount
	let totalAmount = 0
	expenses.forEach((expense) => {
		if (expense.isCredit) {
			totalAmount += expense.amount
		} else {
			totalAmount -= expense.amount
		}
	})	
	
	if(stateSort.state == 1 || stateSort.state == 2) {
		if(stateSort.name == "Date")
			expenses.sort((expenseA, expenseB) => {
				return (stateSort.state == 1 ? expenseA.createdAt - expenseB.createdAt : expenseB.createdAt - expenseA.createdAt)
			})
		else if(stateSort.name == "Name")
			expenses.sort((expenseA, expenseB) => {
				return (stateSort.state == 1 ? expenseA.name.localeCompare(expenseB.name) : expenseB.name.localeCompare(expenseA.name))
			})
		else
			expenses.sort((expenseA, expenseB) => {
				if (expenseA.isCredit && expenseB.isCredit)
					return (stateSort.state == 1 ? expenseA.amount - expenseB.amount : expenseB.amount - expenseA.amount)
				else if (!expenseA.isCredit && !expenseB.isCredit)
					return (stateSort.state == 1 ? expenseB.amount - expenseA.amount : expenseA.amount - expenseB.amount)
				else if (expenseA.isCredit && !expenseB.isCredit)
					return (stateSort.state == 1 ? 1 : -1)
				else
					return (stateSort.state == 1 ? -1 : 1)
			})
	} else {
		expenses.sort((expenseA, expenseB) => expenseB.createdAt - expenseA.createdAt)
	}

	const onPressExpense = (id: string) => {
		const expense = expenses.find(n => n.id === id)
		if (expense) {
			navigation.navigate('ExpenseScreen', { expense })
		}
	}

	const onPressMore = (index: number) => {
		setLineExplanation({ ...lineExplanation, [index]: (lineExplanation[index]) ? 0 : 1 })
	}

	const getExplanationIcon = (idCategory: string) => {
		const category = categories.find(category => category.id == idCategory)
		if (category) {
			return (
				<View style={[styles.containerIcon, { backgroundColor: category.color }]}>
					<Ionicons style={styles.icon} name={category.icon} size={35} color={"white"} />
				</View>
			)
		}
	}

	const getSortIcon = (sortType: string) => {
		if (stateSort.name == sortType && stateSort.state == 1)
			return <Ionicons name="caret-up-outline" size={20}></Ionicons>
		else if (stateSort.name == sortType && stateSort.state == 2)
			return <Ionicons name="caret-down-outline" size={20}></Ionicons>
	}

	const setSortType = (name: string) => {
		if(stateSort.name == name) 
			setStateSort({...stateSort, state: (stateSort.state + 1)%3})
		else
			setStateSort({name: name, state: 1})
	}

	return (
		<View style={styles.expenseList}>
			<View style={styles.containerTitle}>
				<Text style={styles.title}>Transactions : </Text>
				<Text style={styles.title}>$ {totalAmount}</Text>
			</View>

			<View style={styles.containerSortButton}>
				<Text style={styles.titleSort}>Sort by :</Text>
				<TouchableOpacity style={styles.buttonSort} onPress={() => setSortType("Date")}>
					<Text style={styles.textSort}>Date </Text>
					{getSortIcon("Date")}
				</TouchableOpacity>
				<TouchableOpacity style={styles.buttonSort} onPress={() => setSortType("Name")}>
					<Text style={styles.textSort}>Name </Text>
					{getSortIcon("Name")}
				</TouchableOpacity>
				<TouchableOpacity style={styles.buttonSort} onPress={() =>  setSortType("Amount")}>
					<Text style={styles.textSort}>Amount </Text>
					{getSortIcon("Amount")}
				</TouchableOpacity>
			</View>

			<FlatList
				data={expenses}
				renderItem={({ item, index }) => {
					return (
						<TouchableOpacity style={styles.containerExpense} onPress={() => onPressExpense(item.id!)}>
							{getExplanationIcon(item.idCategory)}
							<View style={styles.containerText} >
								<View style={styles.containerLeftText}>
									<Text style={[styles.textExpense, styles.textExpenseFirstLine]}>{item.name}</Text>
									<Text style={styles.textExpense} numberOfLines={lineExplanation[index]}>{item.explanation}</Text>
								</View>
								<View style={styles.containerCenterText}>
									<Text style={[styles.textExpense, styles.textExpenseFirstLine]}>${item.isCredit ? item.amount : ` -${item.amount}`}</Text>
									<Text style={styles.textExpense}>{new Date(item.createdAt).toLocaleDateString()}</Text>
								</View>
								<View style={styles.containerRigthText}>
									<Ionicons style={styles.icon} name='create-outline' size={25} color={"white"} onPress={() => onPressExpense(item.id!)} />
									<Ionicons style={styles.icon} name={(lineExplanation[index]) ? 'chevron-down-outline' : 'chevron-up-outline'} size={25} color={"white"} onPress={() => onPressMore(index)} />
								</View>
							</View>
						</TouchableOpacity>
					)
				}}
			/>
		</View>
	)
}

const styles = StyleSheet.create({
	expenseList: {
		flexDirection: 'column',
		backgroundColor: '#303236',
		paddingBottom: 30,
		paddingTop: 10,
	},
	containerTitle: {
		flexDirection: 'row',
		justifyContent: "space-between",
		paddingLeft: 10,
		paddingRight: 10,
		marginBottom: 10,
	},
	title: {
		fontSize: 25,
		fontWeight: 'bold',
		color: "white",
	},
	containerSortButton: {
		flexDirection: "row",
		justifyContent: "space-around",
		paddingBottom: 10
	},
	titleSort: {
		fontSize: 20,
		color: 'white'
	},
	textSort: {
		textAlign: "center",
		color: "white",
		fontWeight: "bold",
		fontSize: 16
	},
	buttonSort: {
		minWidth: 85,
		backgroundColor: "#2196F3",
		borderRadius: 15,
		padding: 5,
		flexDirection: "row",
		justifyContent: "center"
	},
	containerExpense: {
		flexDirection: "row",
		backgroundColor: "#474a4f",
		marginLeft: 5,
		marginRight: 5,
		borderRadius: 15,
		marginBottom: 15,
		padding: 6,
		paddingLeft: 8,
		paddingRight: 8
	},
	containerIcon: {
		backgroundColor: "#434a4f",
		borderRadius: 15,
		alignSelf: "center",
		borderWidth: 2,
		padding: 5,
	},
	icon: {
		paddingLeft: 3,
		textAlign: "center"
	},
	containerText: {
		justifyContent: "space-between",
		flexDirection: "row",
		flexShrink: 1,
		width: "100%",
	},
	containerLeftText: {
		flexDirection: "column",
		marginLeft: 14,
		marginRight: 8,
		maxWidth: '56%',
	},
	containerCenterText: {
		flexDirection: "column",
		alignItems: "flex-end",
		flexShrink: 1,
		flex: 1,
	},
	containerRigthText: {
		flexDirection: "column",
		alignItems: "flex-end",
		flexShrink: 1,
		width: '10%',
	},
	textExpense: {
		color: "white",
		fontSize: 18
	},
	textExpenseFirstLine: {
		fontWeight: "bold"
	},
})

export default ExpenseList
