import { Action, action, Thunk, thunk } from 'easy-peasy'
import { firebase } from '@react-native-firebase/auth';
import { StoreModel } from '.'
import { Expense } from '../../types/types'
import services from '../../services'
import { EXPENSES_COLLECTION, USERS_COLLECTION } from '../../constants';

export interface ExpenseModel {
	/**
	 * State: raw data that will be used by the app
	 */
	expenses: Expense[]

	/**
	 * Actions: functions that wil directly modify the state
	 */
	setExpenses: Action<ExpenseModel, Expense[]>

	/**
	 * Thunks: functions that call actions
	 */
	listenExpenses: Thunk<ExpenseModel, void, void, StoreModel, () => void>
	createExpense: Thunk<ExpenseModel, { 
        name: string, 
        isCredit: boolean, 
        amount: number, 
        explanation: string, 
        idCategory: string 
    }, void, StoreModel, Promise<void>>
	updateExpense: Thunk<ExpenseModel, { 
        id: string, 
        name: string, 
        isCredit: boolean, 
        amount: number, 
        explanation: string, 
        idCategory: string 
    }, void, StoreModel, Promise<void>>
    deleteExpense: Thunk<ExpenseModel, {id: string}, void, StoreModel, Promise<void>>
}

const expenseModel: ExpenseModel = {
	expenses: [],

	setExpenses: action((state, payload) => {
		return ({ expenses: [...payload] })
	}),

	listenExpenses: thunk((actions) => {
        const unsubscribe = firebase.firestore()
            .collection(USERS_COLLECTION)
            .doc(firebase.auth().currentUser?.uid)
            .collection(EXPENSES_COLLECTION).onSnapshot(
			(onNext) => {
				const expenses = onNext.docs.map(d => {
					return ({...d.data(), id: d.id }) as Expense
                })
				actions.setExpenses(expenses)
			},
			(onError) => console.warn("listenExpenses error: ", onError)
		)
		return unsubscribe
	}),

	createExpense: thunk(async (actions, payload, { getState }) => {
		const { name, isCredit, amount, explanation, idCategory } = payload
		services.expenses.createExpense(name, isCredit, amount, explanation, idCategory)
	}),

	updateExpense: thunk(async (actions, payload, { getState }) => {
        const {  id, name, isCredit, amount, explanation, idCategory } = payload
		services.expenses.updateExpense(id, name, isCredit, amount, explanation, idCategory)
    }),
    
    deleteExpense: thunk(async (actions, payload, { getState }) => {
        const { id } = payload
        services.expenses.deleteExpense(id)
    })

}

export default expenseModel