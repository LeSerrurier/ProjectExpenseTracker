import profiles, { ProfileModel } from './profile'
import expenses, { ExpenseModel } from './expenses'
import categories, { CategoryModel } from './categories'

export interface StoreModel {
	profiles: ProfileModel,
	expenses: ExpenseModel,
	categories: CategoryModel
}

const storeModel: StoreModel = {
	profiles,
	expenses,
	categories
}

export default storeModel