import { Action, action, Thunk, thunk } from 'easy-peasy'
import { firebase } from '@react-native-firebase/auth';
import { StoreModel } from '.'
import { Category } from '../../types/types'
import { CATEGORIES_COLLECTION } from '../../constants';

export interface CategoryModel {
	/**
	 * State: raw data that will be used by the app
	 */
	categories: Category[]

	/**
	 * Actions: functions that wil directly modify the state
	 */
	setCategories: Action<CategoryModel, Category[]>

	/**
	 * Thunks: functions that call actions
	 */
	listenCategories: Thunk<CategoryModel, void, void, StoreModel, () => void>
}

const categoryModel: CategoryModel = {
	categories: [],

	setCategories: action((state, payload) => {
		return ({ categories: [...payload] })
	}),

	listenCategories: thunk((actions) => {
        const unsubscribe = firebase.firestore().collection(CATEGORIES_COLLECTION).onSnapshot(
			(onNext) => {
				const categories = onNext.docs.map(d => {
					return ({...d.data(), id: d.id }) as Category
                })
				actions.setCategories(categories)
			},
			(onError) => console.warn("listenCategories error: ", onError)
		)
		return unsubscribe
	})

}

export default categoryModel