import { Action, action, Thunk, thunk } from 'easy-peasy'
import { firebase } from '@react-native-firebase/auth';
import { StoreModel } from '.'
import { Profile } from '../../types/types'
import services from '../../services'
import { PROFILE_COLLECTION, USERS_COLLECTION } from '../../constants';

export interface ProfileModel {
	/**
	 * State: raw data that will be used by the app
	 */
	profiles: Profile[]

	/**
	 * Actions: functions that wil directly modify the state
	 */
	setProfiles: Action<ProfileModel, Profile[]>

	/**
	 * Thunks: functions that call actions
	 */
	listenProfiles: Thunk<ProfileModel, void, void, StoreModel, () => void>
	createProfile: Thunk<ProfileModel, { firstName: string, lastName: string, age: number, email: string, }, void, StoreModel, Promise<void>>
	updateProfile: Thunk<ProfileModel, { id: string, firstName: string, lastName: string, age: number }, void, StoreModel, Promise<void>>
	deleteProfile: Thunk<ProfileModel, void, void, StoreModel, Promise<void>>	
}

const profileModel: ProfileModel = {
	profiles: [],

	setProfiles: action((state, payload) => {
		return ({ profiles: [...payload] })
	}),

	listenProfiles: thunk((actions) => {
		const unsubscribe = firebase.firestore()
			.collection(USERS_COLLECTION)
			.doc(firebase.auth().currentUser?.uid)
			.collection(PROFILE_COLLECTION).onSnapshot(
			(onNext) => {
				const profiles = onNext.docs.map(d => {
					return ({...d.data(), id: d.id }) as Profile
				})
				actions.setProfiles(profiles)
			},
			(onError) => console.warn("listenProfiles error: ", onError)
		)
		return unsubscribe
	}),

	createProfile: thunk(async (actions, payload, { getState }) => {
		const { firstName, lastName, age, email } = payload
		services.profile.createProfile(firstName, lastName, age, email)
	}),

	updateProfile: thunk(async (actions, payload, { getState }) => {
		const { id, firstName, lastName, age } = payload
		services.profile.updateProfile(id, firstName, lastName, age)
	}),

	deleteProfile: thunk(async (actions, payload, { getState }) => {
		services.profile.deleteProfile()
	}),
}

export default profileModel