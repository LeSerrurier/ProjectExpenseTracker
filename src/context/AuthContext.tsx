import { firebase, FirebaseAuthTypes } from '@react-native-firebase/auth';
import React, { createContext, useContext, useEffect } from 'react';
import service from "../services/index"

type AuthContextType = {
	isSignedIn: boolean;
	user?: FirebaseAuthTypes.User;
	register: (email: string, password: string, firstName: string, lastName: string, age: number) => Promise<void>;
	signIn: (email: string, password: string) => Promise<void>;
	signOut: () => Promise<void>;
}

const defaultAuthState: AuthContextType = {
	isSignedIn: true,
	register: async () => undefined,
	signIn: async () => undefined,
	signOut: async () => undefined,
}

const AuthContext = createContext<AuthContextType>(defaultAuthState);

export const AuthContextProvider: React.FC = ({ children }) => {
	const [auth, setAuth] = React.useState<{
		user?: FirebaseAuthTypes.User;
		isSignedIn: boolean;
	}>({
		isSignedIn: false,
	})

	useEffect(() => {
		const unsubscribe = firebase.auth().onAuthStateChanged((_user) => {
			if (_user) {
				// user is logged in
				setAuth({
					user: _user,
					isSignedIn: true,
				});
			} else {
				// user is logged out
				setAuth({
					user: undefined,
					isSignedIn: false,
				})
			}
		})

		return unsubscribe;
	}, [])

	const register = async (email: string, password: string, firstName: string, lastName: string, age: number) => {
		await firebase.auth().createUserWithEmailAndPassword(email, password).then(() => {
			service.profile.createProfile(firstName, lastName, age, email)
		})
	}

	const signIn = async (email: string, password: string) => {
		await firebase.auth().signInWithEmailAndPassword(email, password);
	}

	const signOut = async () => {
		await firebase.auth().signOut();
	}

	return (
		<AuthContext.Provider
			value={{
				isSignedIn: auth.isSignedIn,
				user: auth.user,
				register,
				signIn,
				signOut,
			}}>
			{children}
		</AuthContext.Provider>
	)
}

export const useAuth = () => {
	const auth = useContext(AuthContext);
	return auth;
}
