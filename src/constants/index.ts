export const USERS_COLLECTION = 'users'
export const PROFILE_COLLECTION = 'profile'
export const CATEGORIES_COLLECTION = 'categories'
export const EXPENSES_COLLECTION = 'expenses'
